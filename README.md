I didn't added the node-module folder as it was too big to upload. 

Files I included :

Postman collection json -> Todoist_API.postman_collection.json

Postman ENV variables   -> QA-dev.postman_environment.json

Postman workspace -> postmanWorkspace.zip

An example of the HTML report generated -> reportTodoist.html

Test in the last request in the collection (Create Multiple Tasks with predefined values in CSV - csvPostmanTaskData.csv) fails if we don't provide the CSV File - it's expected
